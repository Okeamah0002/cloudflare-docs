---
type: table
pcx_content_type: changelog
title: Scheduled changes
weight: 2
layout: wide
---

# Scheduled changes

{{<table-wrap>}}
<table style="width: 100%">
  <thead>
    <tr>
      <th>Announcement Date</th>
      <th>Release Date</th>
      <th>Release Behavior</th>
      <th>Legacy Rule ID</th>
      <th>Rule ID</th>
      <th>Description</th>
      <th>Comments</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>2024-02-05</td>
      <td>2024-02-12</td>
      <td>Block</td>
      <td>100625</td>
      <td>...901523c0</td>
      <td>Jenkins - Information Disclosure - CVE:CVE-2024-23897</td>
      <td>New Detection</td>
    </tr>
  </tbody>
</table>
{{</table-wrap>}}